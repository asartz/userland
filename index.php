<?php

/**
 * Register the autoloader.
 */

require 'vendor/autoload.php';

// Load configuration parameters from .env file.
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

/**
 * Pick controller that must be engaged and action that must be performed.
 * If something is missing force display of the home page.
 */

$controller = Userland\Libs\Helper::getKey($_GET, 'controller', 'page');
$action = Userland\Libs\Helper::getKey($_GET, 'action', 'home');

/**
 * Init eloquent ORM.
 */

use Illuminate\Database\Capsule\Manager as Capsule;

$databaseOrm = new Capsule;

$databaseOrm->addConnection([
    'driver'    => $_ENV['driver'],
    'host'      => $_ENV['host'],
    'database'  => $_ENV['database'],
    'username'  => $_ENV['username'],
    'password'  => $_ENV['password'],
    'charset'   => $_ENV['charset'],
    'collation' => $_ENV['collation'],
    'prefix'    => $_ENV['prefix'],
]);

$databaseOrm->bootEloquent();

/**
 * Init session engine.
 */

$sessionEngine = new Userland\Libs\SessionEngine();

/**
 * Init templating engine.
 */

$templatingEngine = new Userland\Libs\TemplatingEngine(__DIR__ . $_ENV['viewsFolder'], __DIR__ . $_ENV['cacheFolder']);

/**
 * Init routing engine and route the request.
 */

$routingEngine = new Userland\Libs\RoutingEngine($controller, $action, $sessionEngine, $templatingEngine);
echo $routingEngine->route();