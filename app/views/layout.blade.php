<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Userland is a universal user directory. Come join us!">
    <meta name="author" content="asartz">

    <title>Userland</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bower_vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Render any custom css styles specific to a view  -->
    @yield ('styles')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <!-- Render header -->
    @include ('partials.header')

    @yield ('jumbotron')

    <div class="container" style="margin-top:80px;">
      <!-- Render messages if any -->
      @if (isset($messages) and (!empty($messages)))
        <div class="alert alert-info">
          <ul>
            @foreach ($messages as $key => $value)
              <li>{{ $value }}</li>
            @endforeach
          </ul>
        </div>
      @endif

      <!-- Render validation errors if any -->
      @if (isset($errors) and (!empty($errors)))
        <div class="alert alert-warning">
          <ul>
            @foreach ($errors as $key => $value)
              <li>{{ $value }}</li>
            @endforeach
          </ul>
        </div>
      @endif

      <!-- Render main content flow -->
      @yield ('content')

      <hr>

      <!-- Render footer -->
      @include ('partials.footer')
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/bower_vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/bower_vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/custom_vendor/js/ie10-viewport-bug-workaround.js"></script>

    <!-- Render any custom scripts specific to a view  -->
    @yield ('scripts')
  </body>
</html>