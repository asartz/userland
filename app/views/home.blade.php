@extends ('layout')

@section ('jumbotron')
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Welcome to Userland!</h1>
        <p>This is a universal user directory. Are you listed yet?</p>
      </div>
    </div>
@stop

@section ('content')
    <!-- Row of columns -->
    <div class="row">
      <div class="col-md-4">
        <h2>Awesome</h2>
        <p>You bet!</p>
      </div>
      <div class="col-md-4">
        <h2>Cool</h2>
        <p>Absolutely! </p>
     </div>
      <div class="col-md-4">
        <h2>Easy</h2>
        <p>A couple of clicks and you are done.</p>
      </div>
    </div>
@stop