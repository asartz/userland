<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Userland</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right" method="POST" action="/?controller=user&action=search">
          <div class="form-group">
            <input type="text" name="criterion" placeholder="Enter name or email" class="form-control" value="{{$_POST['criterion']}}">
          </div>
          <button type="submit" class="btn btn-success">Search</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
          @if ($session->get('user') === FALSE)
            <li><a href="/?controller=user&action=signup">Sign up</a></li>
            <li><a href="/?controller=user&action=signin">Sign in</a></li>
          @else
            <li><a href="/?controller=user&action=profile">Profile</a></li>
            <li><a href="/?controller=user&action=signout">Logout</a></li>
          @endif
        </ul>
      </div><!--/.navbar-collapse -->
    </div>
</nav>