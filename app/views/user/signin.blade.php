@extends ('layout')

@section ('content')
  <div class="row">
    <div class="col-md-12">
      {!!Former\Facades\Former::open()->method('POST')->action('http://'.$_SERVER['SERVER_ADDR'].'/index.php?controller=user&action=login') !!}
        {!! Former\Facades\Former::text('email')->required()->maxlength(100) !!}
        {!! Former\Facades\Former::password('password')->required()->maxlength(100) !!}
        {!! Former\Facades\Former::submit() !!}
        {!! Former\Facades\Former::reset() !!}
      {!! Former\Facades\Former::close() !!}
    </div>
  </div>
@stop