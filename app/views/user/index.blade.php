@extends ('layout')

@section ('content')
  <div class="row">
    <div class="col-md-12">
      <table class="table">
      <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Country</th>
        <th>Timezone</th>
      </thead>
      <tbody>
        @foreach ($users as $user)
        <tr>
          <td>{{ $user->name}}</td>
          <td>{{ $user->email}}</td>
          <td>{{ $user->countryname}}</td>
          <td>{{ $user->timezone}}</td>
        </tr>
        @endforeach
      </tbody>
      </table>
    </div>
  </div>
@stop