@extends ('layout')

@section ('styles')
  <style type="text/css">
    #secondary_email {
      display: none;
    }
  </style>
@stop

@section ('scripts')
  <!-- Detect user timezone script -->
  <script src="assets/custom_vendor/js/jstz.min.js"></script>

  <!-- Try to guess user timezone and set dropdown value -->
  <script>
    jQuery( document ).ready(function() {
        var tz = jstz.determine();
        jQuery("#timezone > option").each(function() {
          if (tz.name() === this.text) {
            jQuery("#timezone").val(this.value);
            return false;
          }
        });
    });
  </script>
@stop

@section ('content')
  <div class="row">
    <div class="col-md-12">
      {!! Former\Facades\Former::open()->method('POST')->action('http://'.$_SERVER['SERVER_ADDR'].'/index.php?controller=user&action=register') !!}
        {!! Former\Facades\Former::text('fullname')->required()->maxlength(100) !!}
        {!! Former\Facades\Former::text('email')->required()->maxlength(100) !!}
        {!! Former\Facades\Former::password('password')->required()->maxlength(100) !!}
        {!! Former\Facades\Former::password('password_verify')->required()->maxlength(100) !!}
        {!! Former\Facades\Former::select('country')->required()->options($countries, 'ZZ') !!}
        {!! Former\Facades\Former::select('timezone')->required()->options( Userland\Libs\Helper::getTimezones(), 'ZZ') !!}
        {{-- Honeypot field.  If it is submitted, the submission can be treated as spam.--}}
        <input id="secondary_email" type="text" name="secondary_email" size="100" value="" />
        {!! Former\Facades\Former::submit() !!}
      {!! Former\Facades\Former::close() !!}
    </div>
  </div>
@stop