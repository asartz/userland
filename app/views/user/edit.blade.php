@extends ('layout')

@section ('content')
  <div class="row">
    <div class="col-md-12">
      {!! Former\Facades\Former::open()->method('POST')->action('http://'.$_SERVER['SERVER_ADDR'].'/index.php?controller=user&action=update') !!}
        {!! Former\Facades\Former::text('fullname')->required()->maxlength(100)->value($user->name) !!}
        {!! Former\Facades\Former::text('email')->required()->maxlength(100)->value($user->email) !!}
        {!! Former\Facades\Former::password('password')->maxlength(100)->value('') !!}
        {!! Former\Facades\Former::select('country')->required()->options($countries, $user->country_id) !!}
        {!! Former\Facades\Former::select('timezone')->required()->options( Userland\Libs\Helper::getTimezones(), $user->timezone) !!}
        {!! Former\Facades\Former::submit() !!}
      {!! Former\Facades\Former::close() !!}
    </div>
  </div>
@stop