<?php

namespace Userland\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes excluded in case of mass assignment.
     *
     * @var array
     */

    protected $guarded = ['id', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

}
