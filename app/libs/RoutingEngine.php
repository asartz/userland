<?php

namespace Userland\Libs;

use Userland\Libs\SessionEngine;
use Userland\Libs\TemplatingEngine;

class RoutingEngine
{
    /**
     * Add an entry for each new controller and its actions.
     *
     * @var array
     */
    private $controllers = ['page' => ['home', 'error'], 'user' => ['signup', 'register', 'login', 'profile', 'signin', 'signout', 'search', 'update']];

    /**
     * The controller name.
     *
     * @var string
     */
    private $controller;

    /**
     * The controller action.
     *
     * @var string
     */
    private $action;

    /**
     * The session engine instance.
     *
     * @var \Userland\Libs\SessionEngine
     */
    private $sessionEngine;

    /**
     * The templating engine instance.
     *
     * @var Userland\Libs\TemplatingEngine;
     */
    private $templatingEngine;

    /**
     * Class constructor.
     *
     * @param string           $controller
     * @param string           $action
     * @param SessionEngine    $sessionEngine
     * @param TemplatingEngine $templatingEngine
     */
    public function __construct($controller, $action, SessionEngine $sessionEngine, TemplatingEngine $templatingEngine)
    {
        // Sanity check. If something is wrong, force display of the error page.
        if (!array_key_exists($controller, $this->controllers) or !in_array($action, $this->controllers[$controller]))
        {
            $this->controller = 'page';
            $this->action     = 'error';
        }
        else
        {
            $this->controller = $controller;
            $this->action     = $action;
        }

        $this->sessionEngine    = $sessionEngine;
        $this->templatingEngine = $templatingEngine;
    }

    /**
     * Returns the rendered html of this route.
     *
     * @return string
     */
    public function route()
    {
        switch ($this->controller)
        {
            case 'page':
                $controller = new \Userland\Controllers\PageController($this->sessionEngine, $this->templatingEngine);
                break;
            case 'user':
                $controller = new \Userland\Controllers\UserController($this->sessionEngine, $this->templatingEngine);
                break;
        }

        return $controller->{$this->action}();
    }

}
