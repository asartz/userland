<?php

namespace Userland\Libs\Validators;

use Respect\Validation\Validator as RespectValidator;
use Userland\Libs\Helper;

class UserUpdateValidator extends Validator
{
    /**
     * Validates data.
     *
     * @return boolean
     */
    public function passes()
    {
        if (!RespectValidator::stringType()->length(3, 100)->validate(Helper::getKey($this->attributes, 'fullname')))
        {
            array_push($this->errors, 'Problem with field name. min 3 characters.');
        }

        if (!RespectValidator::stringType()->email()->validate(Helper::getKey($this->attributes, 'email')))
        {
            array_push($this->errors, 'Problem with field email. not a valid email address.');
        }

        // Do not validate password unless we really have to.
        if (Helper::getKey($this->attributes, 'password') !== '')
        {
            if (!RespectValidator::stringType()->length(6, 100)->regex('/[0-9]/')->validate(Helper::getKey($this->attributes, 'password'))) {
                array_push($this->errors, 'Problem with field password. min 6 characters and 1 digit.');
            }
        }

        if (RespectValidator::stringType()->equals('ZZ')->validate(Helper::getKey($this->attributes, 'country')))
        {
            array_push($this->errors, 'Problem with field country. it is required.');
        }

        if (RespectValidator::stringType()->equals('ZZ')->validate(Helper::getKey($this->attributes, 'timezone')))
        {
            array_push($this->errors, 'Problem with field timezone. it is required.');
        }

        return empty($this->errors);
    }
}
