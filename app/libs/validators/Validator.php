<?php

namespace Userland\Libs\Validators;

abstract class Validator
{
    /**
     * Holds array of input data, usually $_POST.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Holds validation errors.
     *
     * @var array
     */
    public $errors;

    /**
     * Class constructor.
     *
     * @param array $attributes The input data.
     */
    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;

        $this->errors = [];
    }
    /**
     * Validates data.
     *
     * @return boolean
     */
    abstract protected function passes();
}
