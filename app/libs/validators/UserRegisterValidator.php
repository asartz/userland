<?php

namespace Userland\Libs\Validators;

use Respect\Validation\Validator as RespectValidator;
use Userland\Libs\Helper;

class UserRegisterValidator extends Validator
{

    /**
     * Validates data.
     *
     * @return boolean
     */
    public function passes()
    {
        // Stop spam bots.
        if (!RespectValidator::stringType()->equals('')->validate(Helper::getKey($this->attributes, 'secondary_email')))
        {
            array_push($this->errors, 'Sorry but registration is closed for robots.');
        }

        if (!RespectValidator::stringType()->length(3, 100)->validate(Helper::getKey($this->attributes, 'fullname')))
        {
            array_push($this->errors, 'Problem with field name. min 3 characters.');
        }

        if (!RespectValidator::stringType()->email()->validate(Helper::getKey($this->attributes, 'email')))
        {
            array_push($this->errors, 'Problem with field email. not a valid email address.');
        }

        if (!RespectValidator::stringType()->length(6, 100)->regex('/[0-9]/')->validate(Helper::getKey($this->attributes, 'password')))
        {
            array_push($this->errors, 'Problem with field password. min 6 characters and 1 digit.');
        }

        if (!RespectValidator::stringType()->identical(Helper::getKey($this->attributes, 'password'))->validate(Helper::getKey($this->attributes, 'password_verify')))
        {
            array_push($this->errors, 'Problem with field password. password and verification do not match.');
        }

        if (RespectValidator::stringType()->equals('ZZ')->validate(Helper::getKey($this->attributes, 'country')))
        {
            array_push($this->errors, 'Problem with field country. it is required.');
        }

        if (RespectValidator::stringType()->equals('ZZ')->validate(Helper::getKey($this->attributes, 'timezone')))
        {
            array_push($this->errors, 'Problem with field timezone. it is required.');
        }

        return empty($this->errors);
    }
}
