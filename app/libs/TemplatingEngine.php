<?php

namespace Userland\Libs;

use Philo\Blade\Blade;

class TemplatingEngine
{

    /**
     * The full path to where our views are stored.
     *
     * @var string
     */
    private $viewsFolder;

    /**
     * The full path to where our cached views are stored.
     *
     * @var string
     */
    private $cacheFolder;

    /**
     * Class constructor.
     *
     * @param string $viewsFolder
     * @param string $cacheFolder
     */
    public function __construct($viewsFolder, $cacheFolder)
    {
        $this->viewsFolder = $viewsFolder;
        $this->cacheFolder = $cacheFolder;
    }

    /**
     * Initializes the view.
     *
     * @param string $viewName The name of the view
     * @return \Illuminate\Contracts\View\View
     */
    public function make($viewName)
    {
        $blade = new Blade($this->viewsFolder, $this->cacheFolder);

        return $blade->view()
            ->make($viewName);
    }

}
