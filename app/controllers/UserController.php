<?php

namespace Userland\Controllers;

use Userland\Models\Country;
use Userland\Models\User;
use Userland\Libs\Helper;
use Userland\Libs\Validators\UserRegisterValidator;
use Userland\Libs\Validators\UserUpdateValidator;

class UserController extends BaseController
{
    /**
     * Return the sign in view.
     *
     * @return string
     */
    public function signin()
    {
        return $this->templatingEngine
            ->make(($this->sessionEngine->get('user') === false) ? 'user.signin' : 'unauthorised')
            ->with('session', $this->sessionEngine)
            ->render();
    }

    /**
     * Log the user in.
     *
     * @return string
     */
    public function login()
    {
        $validationErrors = [];
        $messages         = [];
        $usserIsValid     = false;

        $user = User::where('email', Helper::getKey($_POST, 'email'))
            ->first();

        if (count($user) !== 0)
        {
            $hash = $user->password;
            if (password_verify(Helper::getKey($_POST, 'password'), $hash))
            {
                $usserIsValid = true;
            }
        }

        if ($usserIsValid === false)
        {
            array_push($messages, 'Invalid username or password! Please try again.');
            return $this->templatingEngine
                ->make('user.signin')
                ->with(compact('validationErrors'))
                ->with(compact('messages'))
                ->with('session', $this->sessionEngine)
                ->render();

        }
        else
        {
            // Persist authenticated user to session and redirect to home page.
            // Register for 2 hours.
            $this->sessionEngine->register(120);
            $this->sessionEngine->set('user', $user);
            return $this->home();
        }
    }

    /**
     * Log the user out.
     *
     * @return string
     */
    public function signout()
    {
        // Destroy session.
        $this->sessionEngine->end();
        return $this->home();
    }

    /**
     * Display signup view.
     *
     * @return string
     */
    public function signup()
    {
        $countries = Country::lists('name', 'id');

        return $this->templatingEngine
            ->make(($this->sessionEngine->get('user') === false) ? 'user.signup' : 'unauthorised')
            ->with(compact('countries'))
            ->with('session', $this->sessionEngine)
            ->render();
    }

    /**
     * Register new user.
     *
     * @return string
     */
    public function register()
    {
        $countries = Country::lists('name', 'id');

        // Holds any messages that we want to display.
        $messages         = [];

        // Initialise validator instance.
        $validator = new UserRegisterValidator($_POST);

        if ($validator->passes())
        {
            $user             = new User;
            $user->name       = Helper::getKey($_POST, 'fullname');
            $user->email      = Helper::getKey($_POST, 'email');
            $user->password   = password_hash(Helper::getKey($_POST, 'password'), PASSWORD_DEFAULT);
            $user->country_id = Helper::getKey($_POST, 'country');
            $user->timezone   = Helper::getKey($_POST, 'timezone');
            try
            {
                $user->save();
                array_push($messages, 'Thank you for signing up. Please sign in to see if your friends are here too!');
                return $this->templatingEngine
                    ->make('user.registered')
                    ->with(compact('countries'))
                    ->with('errors', $validator->errors)
                    ->with(compact('messages'))
                    ->with('session', $this->sessionEngine)
                    ->render();
            }
            catch (\Exception $e)
            {
                array_push($validator->errors, 'Problem with database commit. ' . $e->getMessage());
                return $this->templatingEngine
                    ->make('user.signup')
                    ->with(compact('countries'))
                    ->with('errors', $validator->errors)
                    ->with(compact('messages'))
                    ->with('session', $this->sessionEngine)
                    ->render();
            }
        }

        return $this->templatingEngine
            ->make('user.signup')
            ->with(compact('countries'))
            ->with('errors', $validator->errors)
            ->with(compact('messages'))
            ->with('session', $this->sessionEngine)
            ->render();
    }

    /**
     * Search user directory.
     *
     * @return string
     */
    public function search()
    {
        $validationErrors = [];
        $messages         = [];

        if ($this->sessionEngine->get('user') === false)
        {
            array_push($messages, 'Please sign in to be able to search');
            $users = [];
        }
        else
        {
            // Return 50 users at most.
            $users = User::join('countries', 'users.country_id', '=', 'countries.id')
                ->where('users.name', 'like', '%' . Helper::getKey($_POST, 'criterion') . '%')
                ->orWhere('users.email', 'like', '%' . Helper::getKey($_POST, 'criterion') . '%')
                ->select('users.name', 'users.email', 'countries.name as countryname', 'users.timezone')
                ->take(50)
                ->get();
        }

        return $this->templatingEngine
            ->make('user.index')
            ->with(compact('users'))
            ->with(compact('validationErrors'))
            ->with(compact('messages'))
            ->with('session', $this->sessionEngine)
            ->render();

    }

    /**
     * Display user profile view.
     *
     * @return string
     */
    public function profile()
    {
        $validationErrors = [];
        $messages         = [];

        $user      = User::find($this->sessionEngine->get('user')->id);
        $countries = Country::lists('name', 'id');

        return $this->templatingEngine
            ->make('user.edit')
            ->with(compact('user'))
            ->with(compact('countries'))
            ->with(compact('validationErrors'))
            ->with(compact('messages'))
            ->with('session', $this->sessionEngine)
            ->render();
    }

    /**
     * Update user profile.
     *
     * @return string
     */
    public function update()
    {
        $countries = Country::lists('name', 'id');

        // Holds any messages that we want to display.
        $messages         = [];

        // Initialise validator instance.
        $validator = new UserUpdateValidator($_POST);

        if ($validator->passes())
        {
            $user        = User::find($this->sessionEngine->get('user')->id);
            $user->name  = Helper::getKey($_POST, 'fullname');
            $user->email = Helper::getKey($_POST, 'email');
            // Do not touch password unless we really have to.
            if (Helper::getKey($_POST, 'password') !== '')
            {
                $user->password = password_hash(Helper::getKey($_POST, 'password'), PASSWORD_DEFAULT);
            }
            $user->country_id = Helper::getKey($_POST, 'country');
            $user->timezone   = Helper::getKey($_POST, 'timezone');
            try
            {
                $user->save();
                array_push($messages, 'Your profile has been updated!');
                return $this->templatingEngine
                    ->make(' home')
                    ->with('errors', $validator->errors)
                    ->with(compact('messages'))
                    ->with('session', $this->sessionEngine)
                    ->render();
            }
            catch (\Exception $e)
            {
                array_push($validator->errors, 'Problem with database commit. ' . $e->getMessage());
                return $this->templatingEngine
                    ->make('user.edit')
                    ->with(compact('countries'))
                    ->with('errors', $validator->errors)
                    ->with(compact('messages'))
                    ->with('session', $this->sessionEngine)
                    ->render();
            }
        }

        return $this->templatingEngine
            ->make('user.edit')
            ->with(compact('countries'))
            ->with('errors', $validator->errors)
            ->with(compact('messages'))
            ->with('session', $this->sessionEngine)
            ->render();

    }
}
