<?php

namespace Userland\Controllers;

use Userland\Libs\SessionEngine;
use Userland\Libs\TemplatingEngine;

abstract class BaseController
{
    /**
     * The session engine instance.
     *
     * @var \Userland\Libs\SessionEngine
     */
    protected $sessionEngine;

    /**
     * The templating engine instance.
     *
     * @var \Userland\Libs\TemplatingEngine
     */
    protected $templatingEngine;

    /**
     * @param SessionEngine $sessionEngine
     * @param TemplatingEngine $templatingEngine
     */
    public function __construct(SessionEngine $sessionEngine, TemplatingEngine $templatingEngine)
    {
        $this->sessionEngine    = $sessionEngine;
        $this->templatingEngine = $templatingEngine;
    }

    /**
     * Return the home view.
     *
     * @return string
     */
    public function home()
    {
        return $this->templatingEngine
            ->make('home')
            ->with('session', $this->sessionEngine)
            ->render();
    }

    /**
     * Return the error view.
     *
     * @return string
     */
    public function error()
    {
        return $this->templatingEngine
            ->make('error')
            ->with('session', $this->sessionEngine)
            ->render();
    }

    /**
     * Return the unauthorized view.
     *
     * @return string
     */
    public function unauthorised()
    {
        return $this->templatingEngine
            ->make('unauthorised')
            ->with('session', $this->sessionEngine)
            ->render();
    }

}
